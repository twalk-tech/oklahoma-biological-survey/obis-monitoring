# AWS provider configuration variables
variable "ACCESS_KEY" {
  type        = string
  description = "The access key ID of an AWS IAM user that has permissions to deploy resources via CICD"
  sensitive   = true
}
variable "SECRET_KEY" {
  type        = string
  description = "The secret key of an AWS IAM user that has permissions to deploy resources via CICD"
  sensitive   = true
}
variable "REGION" {
  type        = string
  description = "The AWS region to deploy resources to"
}

# Application variables
variable "sns_subscription_emails" {
  type        = map(string)
  description = "A map of usernames to email addresses that will receive alerts if OBIS is down"
}
