data "aws_iam_policy_document" "obis_monitoring_lambda_assume_role_policy" {
  statement {
    sid    = "AllowAssumeRoleFromLambda"
    effect = "Allow"

    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com"
      ]
    }
  }
}

data "aws_iam_policy_document" "obis_monitoring_lambda_policy" {
  statement {
    sid    = "AllowOBISMonitoringLambdaInvocation"
    effect = "Allow"

    actions = [
      "lambda:InvokeFunction"
    ]

    resources = [
      "arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:${local.lambda_function_name}:*"
    ]
  }

  statement {
    sid    = "AllowOBISMonitoringCreateLogGroup"
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup"
    ]

    resources = [
      "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*"
    ]
  }

  statement {
    sid    = "AllowOBISMonitoringWriteLogs"
    effect = "Allow"

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = [aws_cloudwatch_log_group.obis_monitoring.arn]
  }

  statement {
    sid    = "AllowOBISMonitorSNSPublish"
    effect = "Allow"

    actions = [
      "sns:Publish"
    ]

    resources = [aws_sns_topic.obis_monitoring.arn]
  }
}

resource "aws_iam_role" "obis_monitoring" {
  name               = "obs-obis-monitoring-lambda-exec-role"
  assume_role_policy = data.aws_iam_policy_document.obis_monitoring_lambda_assume_role_policy.json
}

resource "aws_iam_policy" "obis_monitoring" {
  name   = "obs-obis-monitoring-lambda-iam-policy"
  policy = data.aws_iam_policy_document.obis_monitoring_lambda_policy.json
}

resource "aws_iam_role_policy_attachment" "obis_monitoring" {
  policy_arn = aws_iam_policy.obis_monitoring.arn
  role       = aws_iam_role.obis_monitoring.name
}

moved {
  from = aws_iam_role.obis_monitoring_lambda_exec_role
  to   = aws_iam_role.obis_monitoring
}

moved {
  from = aws_iam_policy.obis_monitoring_lambda_iam_policy
  to   = aws_iam_policy.obis_monitoring
}

moved {
  from = aws_iam_role_policy_attachment.obis_monitoring_lambda_policy_attachment
  to   = aws_iam_role_policy_attachment.obis_monitoring
}
