terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "<5.0.0"
    }
  }

  backend "http" {}
}

provider "aws" {
  access_key = var.ACCESS_KEY
  secret_key = var.SECRET_KEY
  region     = var.REGION
}
