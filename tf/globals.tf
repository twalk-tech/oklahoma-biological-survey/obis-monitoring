locals {
  lambda_function_archive_name = "app_monitor"
  lambda_function_name         = "obis-monitoring"

  # Don't want to expose anybody's email address/phone number, so mark it as sensitive
  sns_subscription_emails = {
    for key, value in var.sns_subscription_emails : key => sensitive(value)
  }
}

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
