resource "aws_lambda_function" "obis_monitoring" {
  function_name = local.lambda_function_name
  description   = "Monitors OBIS. Runs every 10 minutes"
  handler       = "lambda_function.lambda_handler"
  runtime       = "python3.10"

  role    = aws_iam_role.obis_monitoring.arn
  timeout = 300

  filename         = "../.artifacts/${local.lambda_function_archive_name}.zip" # Relative to terraform file
  source_code_hash = filebase64sha256("../.artifacts/${local.lambda_function_archive_name}.zip")

  depends_on = [
    aws_iam_role_policy_attachment.obis_monitoring,
    aws_cloudwatch_log_group.obis_monitoring,
  ]
}

resource "aws_cloudwatch_log_group" "obis_monitoring" {
  name              = "/aws/lambda/${local.lambda_function_name}"
  retention_in_days = 7
}

moved {
  from = aws_lambda_function.obis_monitoring_lambda_function
  to   = aws_lambda_function.obis_monitoring
}

moved {
  from = aws_cloudwatch_log_group.obis_monitoring_lambda_log_group
  to   = aws_cloudwatch_log_group.obis_monitoring
}
