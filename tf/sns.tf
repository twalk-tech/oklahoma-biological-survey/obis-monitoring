resource "aws_sns_topic" "obis_monitoring" {
  name = "obs-obis-monitoring-topic"
}

resource "aws_sns_sms_preferences" "obis_monitoring" {
  default_sender_id   = "OBISMonitor"
  monthly_spend_limit = 1 # Don't spend more than $1/month on SMS messages. Might need to adjust as needed based on usage
}

# NOTE: if an email address is not confirmed outside of Terraform, the AWS resource will not be able to be mamnaged/deleted with Terraform
# As such, would need to be deleted from the AWS console and manually deleted from Terraform state
# See https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_subscription#protocol-support for more details
resource "aws_sns_topic_subscription" "obis_monitoring" {
  for_each = local.sns_subscription_emails

  topic_arn = aws_sns_topic.obis_monitoring.arn
  protocol  = "email" # Requires confirmation outside of Terraform
  endpoint  = each.value
}
