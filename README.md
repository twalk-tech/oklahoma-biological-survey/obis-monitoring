# OBIS Monitoring

This repository defines and deploys an AWS lambda function written in Python that checks if OBIS is operational. If any of the endpoints checked return a failure response, a message is published to an AWS SNS topic that notifies myself (Tyler Walker) via email and text message.
