"""Defines utility functions for the AWS lambda function to monitor applications"""
import boto3
import requests

REQUEST_TIMEOUT=30

class HTTPGetException(Exception):
  """HTTPGetException is used when making a get request to a URL fails"""
  pass

def get_sns_client(): # pragma: no cover
  """Returns a new boto3 SNS client"""
  return boto3.client('sns')

def make_get_request(url: str) -> tuple[int, dict]:
  """Makes a get request"""
  try:
    resp = requests.get(url, timeout=REQUEST_TIMEOUT)
  except requests.exceptions.ConnectionError:
    raise HTTPGetException(f'Could not connect to {url}')
  except requests.exceptions.Timeout:
    raise HTTPGetException(f'Timed out after {REQUEST_TIMEOUT} seconds')

  try:
    return resp.status_code, resp.json()
  # pylint: disable=bare-except
  except: # If resp.json() fails, then simply return an empty dictionary
    return resp.status_code, {}
