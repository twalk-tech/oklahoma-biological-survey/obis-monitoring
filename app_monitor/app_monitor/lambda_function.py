"""Defines the lambda function to monitor applications"""
import re

from utils import HTTPGetException, make_get_request, get_sns_client

SNS_TOPIC_ARN_RE = r'^arn:aws:sns:us-east-2:\d{12}:([\w\-]{1,256}|[\w\-]{1,251}\.fifo)$'

def publish_sns_message(sns, sns_topic_arn: str, msg: str, application_name: str):
  """Publishes a message to SNS with the provided topic ARN and message"""
  published = False
  sns_resp = sns.publish(TopicArn=sns_topic_arn, Message=msg, Subject=f'{application_name} is down')

  if 'MessageId' not in sns_resp:
    raise Exception('Error when publishing to SNS')

  # If an exception was not raised from `publish_sns_message`, then the message was published successfully
  published = True

  return published

def monitor_app(application_name: str, urls: list, sns_topic_arn: str) -> tuple[bool, list]:
  """Runs checks to ensure the application is running"""
  sns = get_sns_client()
  published = False
  errors = []

  for url in urls:
    try:
      status_code, json = make_get_request(url)
    except HTTPGetException as e:
      errors.append(str(e))
    else:
      if status_code not in (200, -1):
        errors.append(f'Failure at URL {url} with response {json}')

  if len(errors) > 0:
    msg = f'{application_name} is down: \n\n'

    for error in errors:
      msg += f'{repr(error)}\n'

    published = publish_sns_message(sns, sns_topic_arn, msg, application_name)

  return published, errors

def lambda_handler(event, context):
  """Default AWS lambda handler function"""
  if 'application_name' not in event:
    raise Exception('Key "application_name" is missing from payload')

  if 'topic_arn' not in event:
    raise Exception('Key "topic_arn" is missing from payload')

  if 'urls' not in event:
    raise Exception('Key "urls" is missing from payload')

  if event['application_name'] == '':
    raise Exception('Must provide the application name')

  if event['topic_arn'] == '':
    raise Exception('Must provide an SNS topic ARN')

  if event['urls'] == []:
    raise Exception('Must provide a list of URLs to monitor')

  sns_topic_arn_pattern = re.compile(SNS_TOPIC_ARN_RE)

  if not sns_topic_arn_pattern.match(event['topic_arn']):
    raise Exception('AWS SNS topic ARN is invalid')

  monitor_app(event['application_name'], event['urls'], event['topic_arn'])
