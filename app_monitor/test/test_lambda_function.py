from .context import *
import unittest

from app_monitor import lambda_function
from .testing_utils import mocked_get_sns_client, mocked_get_sns_client_should_error, mocked_requests_get, mocked_requests_get_connection_error, application_name, test_urls, topic_arn
from unittest import mock

class TestLambdaFunction(unittest.TestCase):
  @mock.patch('app_monitor.lambda_function.get_sns_client', side_effect=mocked_get_sns_client)
  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get)
  def test_lambda_handler_valid_payload(self, mocked_requests_get, mocked_get_sns_client):
    try:
      lambda_function.lambda_handler({'application_name': application_name, 'topic_arn': topic_arn, 'urls': test_urls}, {})
      self.assertEqual(len(mocked_requests_get.call_args_list), len(test_urls))
    except Exception: # pragma: no cover
      self.fail('Invalid payload')

  def test_lambda_handler_invalid_payload_missing_application_name(self):
    with self.assertRaises(Exception) as e:
      lambda_function.lambda_handler({'topic_arn': topic_arn, 'urls': test_urls}, {})

    self.assertEqual(str(e.exception), 'Key "application_name" is missing from payload')

  def test_lambda_handler_invalid_payload_missing_topic_arn(self):
    with self.assertRaises(Exception) as e:
      lambda_function.lambda_handler({'application_name': application_name, 'urls': test_urls}, {})

    self.assertEqual(str(e.exception), 'Key "topic_arn" is missing from payload')

  def test_lambda_handler_invalid_payload_missing_urls(self):
    with self.assertRaises(Exception) as e:
      lambda_function.lambda_handler({'application_name': application_name, 'topic_arn': topic_arn}, {})

    self.assertEqual(str(e.exception), 'Key "urls" is missing from payload')

  def test_lambda_handler_invalid_payload_empty_application_name(self):
    with self.assertRaises(Exception) as e:
      lambda_function.lambda_handler({'application_name': '', 'topic_arn': topic_arn, 'urls': test_urls}, {})

    self.assertEqual(str(e.exception), 'Must provide the application name')

  def test_lambda_hander_invalid_payload_empty_topic_arn(self):
    with self.assertRaises(Exception) as e:
      lambda_function.lambda_handler({'application_name': application_name, 'topic_arn': '', 'urls': test_urls}, {})

    self.assertEqual(str(e.exception), 'Must provide an SNS topic ARN')

  def test_lambda_hander_invalid_payload_empty_urls(self):
    with self.assertRaises(Exception) as e:
      lambda_function.lambda_handler({'application_name': application_name, 'topic_arn': topic_arn, 'urls': []}, {})

    self.assertEqual(str(e.exception), 'Must provide a list of URLs to monitor')

  def test_lambda_handler_invalid_payload_invalid_topic_arn(self):
    with self.assertRaises(Exception) as e:
      lambda_function.lambda_handler({'application_name': application_name, 'topic_arn': 'arn:aws:vpc:xxxxx', 'urls': test_urls}, {})

    self.assertEqual(str(e.exception), 'AWS SNS topic ARN is invalid')

  @mock.patch('app_monitor.lambda_function.get_sns_client', side_effect=mocked_get_sns_client)
  def test_get_sns_client(self, mocked_get_sns_client):
    res = lambda_function.get_sns_client()
    self.assertEqual(res.name, 'sns')
    self.assertEqual(res.should_error, False)

  @mock.patch('app_monitor.lambda_function.get_sns_client', side_effect=mocked_get_sns_client_should_error)
  def test_get_sns_client_should_error(self, mocked_get_sns_client_should_error):
    res = lambda_function.get_sns_client()
    self.assertEqual(res.name, 'sns')
    self.assertEqual(res.should_error, True)

  @mock.patch('app_monitor.lambda_function.get_sns_client', side_effect=mocked_get_sns_client)
  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get)
  def test_monitor_app_success(self, mocked_requests_get, mocked_get_sns_client):
    published, errors = lambda_function.monitor_app(application_name, test_urls, topic_arn)
    self.assertFalse(published)
    self.assertEqual(len(errors), 0)
    self.assertEqual(len(mocked_requests_get.call_args_list), len(test_urls))
    self.assertEqual(len(mocked_get_sns_client.call_args_list), 1)

  @mock.patch('app_monitor.lambda_function.get_sns_client', side_effect=mocked_get_sns_client)
  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get)
  def test_monitor_app_failure(self, mocked_requests_get, mocked_get_sns_client):
    # Make a copy of the test_urls array by value, rather than reference
    test_urls_with_error = test_urls[:]
    test_urls_with_error.append('https://cool-site.com/unhealthy')
    published, errors = lambda_function.monitor_app(application_name, test_urls_with_error, topic_arn)
    self.assertTrue(published)
    self.assertEqual(len(errors), 1)
    self.assertEqual(len(mocked_requests_get.call_args_list), len(test_urls_with_error))
    self.assertEqual(len(mocked_get_sns_client.call_args_list), 1)

  @mock.patch('app_monitor.lambda_function.get_sns_client', side_effect=mocked_get_sns_client_should_error)
  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get)
  def test_monitor_app_publish_failure(self, mocked_requests_get, mocked_get_sns_client):
    # Make a copy of the test_urls array by value, rather than reference
    test_urls_with_error = test_urls[:]
    test_urls_with_error.append('https://cool-site.com/unhealthy')
    with self.assertRaises(Exception) as e:
      lambda_function.monitor_app(application_name, test_urls_with_error, topic_arn)

    self.assertEqual(str(e.exception), 'Error when publishing to SNS')

  @mock.patch('app_monitor.lambda_function.get_sns_client', side_effect=mocked_get_sns_client)
  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get_connection_error)
  def test_monitor_app_http_get_error(self, mocked_requests_get, mocked_get_sns_client):
    url = 'https://connection-error-site.com/'
    test_urls_connection_error = [url]
    published, errors = lambda_function.monitor_app(application_name, test_urls_connection_error, topic_arn)
    self.assertTrue(published)
    self.assertEqual(len(errors), 1)
    self.assertEqual(len(mocked_requests_get.call_args_list), len(test_urls_connection_error))
    self.assertIn(f'Could not connect to {url}', errors)

if __name__ == '__main__': # pragma: no cover
  unittest.main()
