import json
import requests

application_name = 'cool-application'
topic_arn = 'arn:aws:sns:us-east-2:123456789012:topic'
test_urls = ['https://cool-site.com/', 'https://cool-site.com/healthy']

test_responses = {
  'https://cool-site.com/': {
    'data': 'no_json',
    'status_code': 200
  },
  'https://cool-site.com/no-json-healthy': {
    'data': 'no_json',
    'status_code': 200
  },
  'https://cool-site.com/no-json-unhealthy': {
    'data': 'no_json',
    'status_code': 500
  },
  'https://cool-site.com/healthy': {
    'data': {
      'healthy': True
    },
    'status_code': 200
  },
  'https://cool-site.com/unhealthy': {
    'data': {
      'healthy': False
    },
    'status_code': 500
  }
}

class TestSNSClient():# pragma: no cover
  def __init__(self, name: str, should_error: bool = False) -> None:
    self._name = name
    self._should_error = should_error

  @property
  def name(self):
    return self._name

  @property
  def should_error(self):
    return self._should_error

  def publish(self, TopicArn, Message, Subject):
    if self.should_error:
      return {}
    return { 'MessageId': '1234567890' }

def mocked_get_sns_client(): # pragma: no cover
  return TestSNSClient('sns')

def mocked_get_sns_client_should_error(): # pragma: no cover
  return TestSNSClient('sns', True)

class MockResponse():
  def __init__(self, json_data, status_code):
    self._json_data = json_data
    self._status_code = status_code

  @property
  def status_code(self):
    return self._status_code

  def json(self):
    if self._json_data == 'no_json':
      raise json.decoder.JSONDecodeError(msg='msg', doc='doc', pos=0)
    return self._json_data

def mocked_requests_get(url: str, timeout: int): # pragma: no cover
  return MockResponse(test_responses[url]['data'], test_responses[url]['status_code'])

def mocked_requests_get_connection_error(url: str, timeout: int): # pragma: no cover
  raise requests.exceptions.ConnectionError()

def mocked_requests_get_timeout(url: str, timeout: int): # pragma: no cover
  raise requests.exceptions.Timeout()
