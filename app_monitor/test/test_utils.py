import unittest

from app_monitor import utils
from .testing_utils import mocked_requests_get, mocked_requests_get_connection_error, mocked_requests_get_timeout
from unittest import mock

class TestUtils(unittest.TestCase):
  def setUp(self):
    utils.boto3.client = mock.MagicMock()

  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get)
  def test_get_sns_client(self, mocked_requests_get):
    client = utils.get_sns_client()
    utils.boto3.client.assert_called_once_with('sns')
    self.assertIsInstance(client, mock.MagicMock)

  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get)
  def test_make_get_request_root_url(self, mocked_requests_get):
    status_code, json = utils.make_get_request('https://cool-site.com/')
    self.assertEqual(status_code, 200)
    self.assertEqual(json, {})
    self.assertIn(mock.call('https://cool-site.com/', timeout=30), mocked_requests_get.call_args_list)
    self.assertEqual(len(mocked_requests_get.call_args_list), 1)

  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get)
  def test_make_get_request_no_json_healthy(self, mocked_requests_get):
    status_code, json = utils.make_get_request('https://cool-site.com/no-json-healthy')
    self.assertEqual(status_code, 200)
    self.assertEqual(json, {})
    self.assertIn(mock.call('https://cool-site.com/no-json-healthy', timeout=30), mocked_requests_get.call_args_list)
    self.assertEqual(len(mocked_requests_get.call_args_list), 1)

  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get)
  def test_make_get_request_no_json_unhealthy(self, mocked_requests_get):
    status_code, json = utils.make_get_request('https://cool-site.com/no-json-unhealthy')
    self.assertEqual(status_code, 500)
    self.assertEqual(json, {})
    self.assertIn(mock.call('https://cool-site.com/no-json-unhealthy', timeout=30), mocked_requests_get.call_args_list)
    self.assertEqual(len(mocked_requests_get.call_args_list), 1)

  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get)
  def test_make_get_request_json_healthy(self, mocked_requests_get):
    status_code, json = utils.make_get_request('https://cool-site.com/healthy')
    self.assertEqual(status_code, 200)
    self.assertEqual(json, {'healthy': True})
    self.assertIn(mock.call('https://cool-site.com/healthy', timeout=30), mocked_requests_get.call_args_list)
    self.assertEqual(len(mocked_requests_get.call_args_list), 1)

  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get)
  def test_make_get_request_json_unhealthy(self, mocked_requests_get):
    status_code, json = utils.make_get_request('https://cool-site.com/unhealthy')
    self.assertEqual(status_code, 500)
    self.assertEqual(json, {'healthy': False})
    self.assertIn(mock.call('https://cool-site.com/unhealthy', timeout=30), mocked_requests_get.call_args_list)
    self.assertEqual(len(mocked_requests_get.call_args_list), 1)

  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get_connection_error)
  def test_make_get_request_connection_error(self, mocked_requests_get):
    url = 'https://connection-error-site.com/'
    with self.assertRaises(utils.HTTPGetException) as e:
      utils.make_get_request(url)

    self.assertEqual(str(e.exception), f'Could not connect to {url}')

  @mock.patch('app_monitor.utils.requests.get', side_effect=mocked_requests_get_timeout)
  def test_make_get_request_timeout(self, mocked_requests_get):
    with self.assertRaises(utils.HTTPGetException) as e:
      utils.make_get_request('https://timeout-site.com/')

    self.assertEqual(str(e.exception), 'Timed out after 30 seconds')

if __name__ == '__main__': # pragma: no cover
  unittest.main()
