# Changelog

## 0.1.6 (2024-03-12)

### fixed (1 change)

- [Increasing request timeout to 30 seconds](twalk-tech/oklahoma-biological-survey/obis-monitoring@e5dd6f607f8bd9ba4e90c7c1b3e390d6f120e02b) ([merge request](twalk-tech/oklahoma-biological-survey/obis-monitoring!67))

## 0.1.5 (2024-01-08)

### fixed (1 change)

- [Updating SNS topic subscriptions to send emails rather than SMS messages](twalk-tech/oklahoma-biological-survey/obis-monitoring@dfa9267dbcec51952908205fa2401e302c7ec0b1) ([merge request](twalk-tech/oklahoma-biological-survey/obis-monitoring!58))

## 0.1.4 (2024-01-08)

### fixed (1 change)

- [Allowing OBIS Monitoring lambda policy to publish to the SNS topic](twalk-tech/oklahoma-biological-survey/obis-monitoring@6fd167351cba3ca6965e00f95d961d35689c854b) ([merge request](twalk-tech/oklahoma-biological-survey/obis-monitoring!56))

## 0.1.3 (2024-01-05)

### fixed (1 change)

- [fixing order of arguments](twalk-tech/oklahoma-biological-survey/obis-monitoring@1e181542e8143d3c1788b76dfa88be32fa8b9312) ([merge request](twalk-tech/oklahoma-biological-survey/obis-monitoring!49))

## 0.1.2 (2024-01-05)

### fixed (1 change)

- [Adding comment](twalk-tech/oklahoma-biological-survey/obis-monitoring@b0a027db512cce9b8ecbe34743129eb03f0dd325) ([merge request](twalk-tech/oklahoma-biological-survey/obis-monitoring!45))

## 0.1.1 (2024-01-05)

### fixed (1 change)

- [importing from .utils rather than app_monitor.utils](twalk-tech/oklahoma-biological-survey/obis-monitoring@bde4b51b13ededd5b599d498e9c76345e3f1338e) ([merge request](twalk-tech/oklahoma-biological-survey/obis-monitoring!37))

## 0.1.0 (2024-01-03)

### added (2 changes)

- [Update README](twalk-tech/oklahoma-biological-survey/obis-monitoring@ece1142ed90a7f4fe2d33728c6c19f79ee41f065) ([merge request](twalk-tech/oklahoma-biological-survey/obis-monitoring!13))
- [using template from templates repo](twalk-tech/oklahoma-biological-survey/obis-monitoring@1ee91c7f294c72a27b3afeec2df2e05bebf9f085) ([merge request](twalk-tech/oklahoma-biological-survey/obis-monitoring!6))
